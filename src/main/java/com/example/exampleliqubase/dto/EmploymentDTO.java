package com.example.exampleliqubase.dto;

import java.time.LocalDate;

public class EmploymentDTO {

    private long employmentId;
    private LocalDate startDt;
    private LocalDate endDt;
    private long workTypeId;
    private String organizationName;
    private String organizationAddress;
    private String positionName;
    private long personId;

    public long getEmploymentId() {
        return employmentId;
    }

    public void setEmploymentId(long employmentId) {
        this.employmentId = employmentId;
    }

    public LocalDate getStartDt() {
        return startDt;
    }

    public void setStartDt(LocalDate startDt) {
        this.startDt = startDt;
    }

    public LocalDate getEndDt() {
        return endDt;
    }

    public void setEndDt(LocalDate endDt) {
        this.endDt = endDt;
    }

    public long getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(long workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAddress() {
        return organizationAddress;
    }

    public void setOrganizationAddress(String organizationAddress) {
        this.organizationAddress = organizationAddress;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }
}
