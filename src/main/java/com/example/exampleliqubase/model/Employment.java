package com.example.exampleliqubase.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Employment {

    private long employmentId;
    private int version;
    private LocalDate startDt;
    private LocalDate endDt;
    private long workTypeId;
    private String organizationName;
    private String organizationAddress;
    private String positionName;
    private long personId;



    public long getEmploymentId() {
        return employmentId;
    }

    public void setEmploymentId(long employmentId) {
        this.employmentId = employmentId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDate getStartDt() {
        return startDt;
    }

    public void setStartDt(LocalDate startDt) {
        this.startDt = startDt;
    }

    public LocalDate getEndDt() {
        return endDt;
    }

    public void setEndDt(LocalDate endDt) {
        this.endDt = endDt;
    }

    public long getWorkTypeId() {
        return workTypeId;
    }

    public void setWorkTypeId(long workTypeId) {
        this.workTypeId = workTypeId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAddress() {
        return organizationAddress;
    }

    public void setOrganizationAddress(String organizationAddress) {
        this.organizationAddress = organizationAddress;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employment that = (Employment) o;
        return employmentId == that.employmentId && version == that.version && workTypeId == that.workTypeId && personId == that.personId && Objects.equals(startDt, that.startDt) && Objects.equals(endDt, that.endDt) && organizationName.equals(that.organizationName) && organizationAddress.equals(that.organizationAddress) && positionName.equals(that.positionName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employmentId, version, startDt, endDt, workTypeId, organizationName, organizationAddress, positionName, personId);
    }
}
