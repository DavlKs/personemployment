package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.Employment;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface EmploymentMapper {

    @Insert("Insert into Employment (version, start_dt, end_dt, work_type_id, " +
            "organization_name, organization_address, position_name, person_id) " +
            "values (#{version}, #{startDt}, #{endDt}, #{workTypeId}, #{organizationName}, " +
            "#{organizationAddress}, #{positionName}, #{personId})")
    void addEmployment (Employment employment);

    @Update("Update Employment set version= #{version}, start_dt = #{startDt}, " +
            "end_dt = #{endDt}, work_type_id = #{workTypeId}, organization_name = #{organizationName}, " +
            "organization_address = #{organizationAddress}, position_name = #{positionName} " +
            "where employment_id = #{employmentId}")
    void updateEmployment(Employment employment);

    @Delete("Delete from Employment where employment_id=#{employmentId}")
    void deleteEmploymentById(long employmentId);

    @Select("SELECT employment_id, version, start_dt, end_dt, work_type_id, " +
            "organization_name, organization_address, position_name, person_id " +
            "FROM Employment WHERE employment_id = #{employmentId}")
    Employment getEmployment(long employmentId);

    @Select("SELECT employment_id, version, start_dt, end_dt, work_type_id, " +
            "organization_name, organization_address, position_name, person_id "  +
            "FROM Employment WHERE person_id = #{personId}")
    List<Employment> getAllEmploymentsByPersonId(long personId);


}

