package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.Employment;
import com.example.exampleliqubase.model.Person;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import java.util.List;

@Mapper
@Repository
public interface PersonMapper {

    @Insert("Insert into Person (first_name, last_name, middle_name, birth_date, gender) " +
            "values (#{firstName}, #{lastName}, #{middleName}, #{birthDate}, #{gender})")
    void addPerson (Person person);

    @Update("Update Person set first_name= #{firstName}, last_name = #{lastName}, " +
            "middle_name = #{middleName}, birth_date = #{birthDate}, gender = #{gender} " +
            "where person_id= #{personId}")
    void updatePerson(Person person);

    @Delete("Delete from Person where person_id=#{personId}")
    void deletePersonById(long personId);

    @Select("SELECT person_id, first_name, last_name, middle_name, birth_date, gender " +
            "FROM Person WHERE person_id = #{personId}")
    Person getPerson(long personId);

    @Select("SELECT * FROM Person")
    List<Person> getAllPersons();
}
