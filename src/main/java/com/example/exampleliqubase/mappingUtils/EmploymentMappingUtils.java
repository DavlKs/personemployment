package com.example.exampleliqubase.mappingUtils;

import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.Employment;

public class EmploymentMappingUtils {

    public static EmploymentDTO mapToEmploymentDTO(Employment entity) {

        EmploymentDTO dto = new EmploymentDTO();
        dto.setEmploymentId(entity.getEmploymentId());
        dto.setStartDt(entity.getStartDt());
        dto.setEndDt(entity.getStartDt());
        dto.setOrganizationName(entity.getOrganizationName());
        dto.setOrganizationAddress(entity.getOrganizationAddress());
        dto.setWorkTypeId(entity.getWorkTypeId());
        dto.setPositionName(entity.getPositionName());
        dto.setPersonId(entity.getPersonId());
        return dto;

    }

    public static Employment mapToEmployment(EmploymentDTO dto) {

        Employment entity = new Employment();
        entity.setEmploymentId(dto.getEmploymentId());
        entity.setStartDt(dto.getStartDt());
        entity.setEndDt(dto.getEndDt());
        entity.setOrganizationAddress(dto.getOrganizationAddress());
        entity.setOrganizationName(dto.getOrganizationName());
        entity.setWorkTypeId(dto.getWorkTypeId());
        entity.setPositionName(dto.getPositionName());
        entity.setPersonId(dto.getPersonId());
        return entity;
    }
}
