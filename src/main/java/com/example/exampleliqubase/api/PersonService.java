package com.example.exampleliqubase.api;

import com.example.exampleliqubase.model.Employment;
import com.example.exampleliqubase.model.Person;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PersonService {


    void addPerson (Person person);


    void updatePerson(Person person);


    void deletePersonById(long personId);


    Person getPerson(long personId);


    List<Person> getAllPersons();
}
