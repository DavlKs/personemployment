package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.Employment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmploymentService {

    void addEmployment (Employment employment);


    void updateEmployment(Employment employment);


    void deleteEmploymentById(long employmentId);

    EmploymentDTO getEmployment(long employmentId);

    List<EmploymentDTO> getAllEmploymentsByPersonId(long personId);

    void unpackEmploymentList(List<EmploymentDTO> list, long personId);
}
