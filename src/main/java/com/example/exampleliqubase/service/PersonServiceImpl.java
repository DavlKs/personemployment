package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.PersonService;
import com.example.exampleliqubase.dao.PersonMapper;
import com.example.exampleliqubase.model.Employment;
import com.example.exampleliqubase.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PersonServiceImpl implements PersonService {

    private PersonMapper personMapper;

    @Autowired
    public PersonServiceImpl(PersonMapper personMapper) {
        this.personMapper = personMapper;
    }

    @Override
    public void addPerson(Person person) {
        personMapper.addPerson(person);
    }

    @Override
    public void updatePerson(Person person) {
        personMapper.updatePerson(person);
    }

    @Override
    public void deletePersonById(long personId) {
        personMapper.deletePersonById(personId);
    }

    @Override
    public Person getPerson(long personId) {
        return personMapper.getPerson(personId);
    }


    @Override
    public List<Person> getAllPersons() {
        return personMapper.getAllPersons();
    }


}
