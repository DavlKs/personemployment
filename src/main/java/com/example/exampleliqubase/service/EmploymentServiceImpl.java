package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dao.PersonMapper;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.mappingUtils.EmploymentMappingUtils;
import com.example.exampleliqubase.model.Employment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmploymentServiceImpl implements EmploymentService {

    private EmploymentMapper employmentMapper;

    @Autowired
    public EmploymentServiceImpl(EmploymentMapper employmentMapper) {
        this.employmentMapper = employmentMapper;
    }

    @Override
    public void addEmployment(Employment employment) {
        employmentMapper.addEmployment(employment);
    }

    @Override
    public void updateEmployment(Employment employment) {
        employmentMapper.updateEmployment(employment);
    }

    @Override
    public void deleteEmploymentById(long employmentId) {
        employmentMapper.deleteEmploymentById(employmentId);
    }

    @Override
    public EmploymentDTO getEmployment(long employmentId) {
        return EmploymentMappingUtils.mapToEmploymentDTO(employmentMapper.getEmployment(employmentId));
    }

    @Override
    public List<EmploymentDTO> getAllEmploymentsByPersonId(long personId) {
        return employmentMapper.getAllEmploymentsByPersonId(personId).stream()
                .map(EmploymentMappingUtils::mapToEmploymentDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void unpackEmploymentList(List<EmploymentDTO> list, long personId) {
        List<Employment> dbEmpList = employmentMapper.getAllEmploymentsByPersonId(personId);
        List <Employment> frontList = list.stream()
                .map(EmploymentMappingUtils::mapToEmployment)
                .collect(Collectors.toList());

        for (Employment frontEmp : frontList) {
            if (frontEmp.getEmploymentId() == 0) {
                addEmployment(frontEmp);
            } else {
                long empId = frontEmp.getEmploymentId();
                Employment dbEmp = employmentMapper.getEmployment(empId);
                if (!frontEmp.equals(dbEmp)) {
                    employmentMapper.updateEmployment(frontEmp);
                }
            }
        }

        for (Employment dbEmp : dbEmpList) {
            if (!frontList.contains(dbEmp)) {
                employmentMapper.deleteEmploymentById(dbEmp.getEmploymentId());
            }
        }
    }

}
