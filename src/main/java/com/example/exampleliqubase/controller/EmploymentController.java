package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.Employment;
import com.example.exampleliqubase.api.EmploymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employment/records")
public class EmploymentController {

    private EmploymentService employmentService;

    @Autowired
    public EmploymentController(EmploymentService employmentService) {
        this.employmentService = employmentService;
    }

    @PostMapping
    void addEmployment(@RequestBody Employment employment){
        employmentService.addEmployment(employment);
    }

    @GetMapping("{employmentId}")
    EmploymentDTO getEmployment(@PathVariable long employmentId){
        return employmentService.getEmployment(employmentId);
    }

    @PutMapping
    void updateEmployment(@RequestBody Employment employment){
        employmentService.updateEmployment(employment);
    }

    @DeleteMapping("{employmentId}")
    void deleteEmployment(@PathVariable long employmentId) {
        employmentService.deleteEmploymentById(employmentId);
    }

    @GetMapping("/person/{personId}")
    public List<EmploymentDTO> getAllEmploymentsByPersonId(@PathVariable long personId) {
        return employmentService.getAllEmploymentsByPersonId(personId);
    }

    @PostMapping("/update/{personId}")
    public void unpackEmploymentList(@RequestBody List<EmploymentDTO> list, @PathVariable long personId) {
        employmentService.unpackEmploymentList(list, personId);
    }

}
