package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.model.Employment;
import com.example.exampleliqubase.model.Person;
import com.example.exampleliqubase.api.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/person/records")
public class PersonController {

    private PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/{personId}")
    Person getPerson(@PathVariable long personId) {
        return personService.getPerson(personId);
    }

    @PostMapping
    void addPerson (@RequestBody Person person) {
        personService.addPerson(person);
    }

    @PutMapping
    void updatePerson(@RequestBody Person person){
        personService.updatePerson(person);
    }

    @DeleteMapping("/{personId}")
    void deletePersonById(@PathVariable long personId){
        personService.deletePersonById(personId);
    }


    @GetMapping
    public List<Person> getAllPersons() {
        return personService.getAllPersons();
    }


}
