create sequence if not exists person_seq start 1;
create sequence if not exists employment_seq start 1;

create table person
(
    person_id   bigint default nextval('person_seq'::regclass) not null
        constraint person_pk
            primary key,
    first_name  varchar(256)                                   not null,
    last_name   varchar(256)                                   not null,
    middle_name varchar(256)                                   not null,
    birth_date  date,
    gender      varchar(100)                                   not null
);

comment on table person is 'Информация о человеке';

comment on column person.person_id is 'Идентификатор.';

comment on column person.first_name is 'Имя';

comment on column person.last_name is 'Фамилия';

comment on column person.middle_name is 'Отчество';

comment on column person.birth_date is 'Дата рождения';

comment on column person.gender is 'Пол';


create table employment
(
    employment_id        bigint default nextval('employment_seq'::regclass) not null
        constraint employment_pk
            primary key,
    version              integer                                            not null,
    start_dt             date,
    end_dt               date,
    work_type_id         bigint,
    organization_name    varchar(256),
    organization_address text,
    position_name        varchar(256),
    person_id            bigint
);

comment on table employment is 'запись о трудовой деятельности.';

comment on column employment.employment_id is 'идентификатор.';

comment on column employment.version is 'реализация оптимистической блокировки';

comment on column employment.start_dt is 'дата начала трудовой деятельности';

comment on column employment.end_dt is 'дата окончания трудовой деятельности';

comment on column employment.work_type_id is 'тип деятельности';

comment on column employment.organization_name is 'наименование организации.';

comment on column employment.organization_address is 'адрес организации.';

comment on column employment.position_name is 'должность.';

alter table employment
    add constraint person_id_fk foreign key (person_id) references person(person_id);
